### Summary
Here we track todo's, which idea we already had, but will look at a later time in the project.

# Power
bei den UV/OV/Reverse Protection
- ein in Reihe geschalteter Kondensator (vielleicht/wahrscheinlich zusätzlich zum großen Stützkondensator, der Back-EMF aufnehmen soll.)


# Security Processor
- insert digital isolator (see draw.io sketch)
- On/Off Button
- oled on security processor?
- debug led's not visible when on pcb instead of front

# Overall
#- Plugdetection for Axis boards and 4-6 axis pcb
- Status-LEDs (for debugging)
- "deactivate" a axis with a jumper
- debug led on motor pins
- debug led on step & dir (maybe 4bit counter ic with 4 leds)


# Motor Controller
- Voltage Divider for Axis Sense wire (8Megaohm zu 1 Megaohm)
